  -baseurl <br>
        Bas url, ex: https://radne.se/classes/<br>
        Kräver avslutande slash / (default " ")<br>
  -column <br>
        What column to iterate through<br>
  -f <br>
        relative or absolute path to csv file (default "validate.csv")<br>
  -seperator <br>
        Csv seperator (default ",")<br>
  -v    <br>
        increase verbosity<br>