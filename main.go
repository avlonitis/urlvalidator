package main

import (
	"crypto/tls"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

func cliParse() (baseURL string, filePath string, verbose bool, seperator string, column int) {
	flag.StringVar(&baseURL, "baseurl", " ", "Baseurl, eg: "+"https://example.com/foo/bar"+"\nDepending on your csv file a trailing slash ( / ) could be neccessary")
	flag.StringVar(&filePath, "f", "validate.csv", "Relative or absolute path to csv file")
	flag.BoolVar(&verbose, "v", false, "Increase verbosity")
	flag.StringVar(&seperator, "seperator", ",", "Csv seperator character")
	flag.IntVar(&column, "column", 0, "What column of csv file to iterate through, arrays start at [0]")

	flag.Parse()
	if baseURL == " " {
		flag.PrintDefaults()
		os.Exit(2)
	}
	return
}

func main() {
	baseURL, filePath, verbose, seperator, column := cliParse()

	file, _ := os.Open(filePath)

	r := csv.NewReader(file)
	runeSeperator := []rune(seperator)
	r.Comma = runeSeperator[0]
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	var errorCounter int
	for {
		record, err := r.Read()

		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
		}
		webPath := strings.ToLower(record[column])
		resp, err := client.Get(baseURL + webPath)
		req, err := http.NewRequest("GET", baseURL+webPath, nil)
		req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36")

		resp, err = client.Do(req)
		if err != nil {
			fmt.Println(err)
		}

		if verbose {
			fmt.Printf("baseURL: %v | Url: %v | Resp: %v \n", baseURL, webPath, resp.StatusCode)
		} else {
			if resp.StatusCode != 200 {
				fmt.Printf("baseURL: %v | Url: %v | Resp: %v \n", baseURL, webPath, resp.StatusCode)
				errorCounter++
			}
		}
		// Drastically reduce memory footprint
		resp = nil
		time.Sleep(500 * time.Millisecond)
	}
	if errorCounter < 1 {
		fmt.Printf("baseURL: %v | No errors found", baseURL)
	}
}
